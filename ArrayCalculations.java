package com.qagroup.ArrayCalculations;

public class ArrayCalculations {

	public static void main(String[] args) {
		
		double array[]={2,5,7,8};
		
		ArrayCalculations prod = new ArrayCalculations();
		double res1 = prod.ArrayProduct(array);
		//double res1=ArraySum(array);
		System.out.println("Sum of array equals: " + res1);
		
		ArrayCalculations sum = new ArrayCalculations();
		double res2 = sum.ArraySum(array);
		//double res2 = ArrayProduct(array);
		System.out.println("Product of array equals: " + res2);
		
		ArrayCalculations Av = new ArrayCalculations();
		double res3 = Av.ArrayAvarage(array);
		//double res3 = ArrayAvarage(array);
		System.out.println("Avarage of array equals: " + res3);
			
	}

    public double ArrayProduct(double ArProd[]){
    	double product=1;
    	
    	for (double element : ArProd)
    		product*=element;
    	
    	return (product);
    }
    
    public double ArraySum(double ArSum[]){
    	int sum=0;
    	
    	for (double element : ArSum)
    		sum+=element;
    	
    	return (sum);
    			
    }
    
    public double ArrayAvarage(double ArAv[]){
    	double sum=0;
    	double avarage;
    	
    	for (double element : ArAv)
    		sum+=element;
    	
    	avarage=sum/ArAv.length;
    	return (avarage);
    } 
}